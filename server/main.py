import asyncio
import json
import websockets

db = {}

async def hello(websocket, path):
    if path == '/device':
        async for raw_data in websocket:
            try:
                data = json.loads(raw_data)
                data_type = data['type']
            except:
                print("Unable to parse JSON")
                continue
            db[data_type] = data
    elif path == '/web':
        while True:
            await websocket.send(json.dumps(db))
            await asyncio.sleep(1)

start_server = websockets.serve(hello, "localhost", 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
