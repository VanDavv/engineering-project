import ctypes
import json

import cv2
from depthai_utils import DepthAI
from modules import PersonTrackerDebug
from multiprocessing import Process, Manager
from websocket import create_connection


d = DepthAI(threshold=0.4)
pt = PersonTrackerDebug()
shared_results = Manager().Value(ctypes.c_wchar_p, '{}')


def notify(payload):
    while True:
        try:
            ws = create_connection("ws://localhost:8765/device")
            while True:
                try:
                    loaded = json.loads(payload.value)
                    if len(loaded) == 0:
                        continue

                    ws.send(json.dumps({
                        "type": "tracker",
                        "data": loaded,
                    }))
                except:
                    break
            ws.close()
        except:
            pass


p = Process(target=notify, args=(shared_results, ))
p.daemon = True
p.start()


for frame, results in d.run():
    for left, top, right, bottom in results:
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
    total = pt.parse(frame, results)
    print(pt.get_directions())
    shared_results.value = json.dumps(pt.get_directions())
    cv2.imshow('previewout', frame)

    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    elif key == ord('r'):
        pt.__init__()


del d
