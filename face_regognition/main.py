import argparse
import os
from glob import glob
from pathlib import Path

import cv2
import dlib
import numpy as np

from depthai_utils import DepthAI

parser = argparse.ArgumentParser()
parser.add_argument('-l', '--learn', default="", type=str, help="Learn person. As a param for this switch provide a name")
args = parser.parse_args()

d = DepthAI(threshold=0.5, fps=3, sync=True)
predictor_path = str((Path(__file__).parent / Path('model/face_shape_predictor.dat')).resolve().absolute())
face_rec_model_path = str((Path(__file__).parent / Path('model/face_recognition_model.dat')).resolve().absolute())
sp = dlib.shape_predictor(predictor_path)
facerec = dlib.face_recognition_model_v1(face_rec_model_path)

face_descriptor = None
known_faces = {os.path.splitext(filename)[0]: dlib.vector(np.load(filename)) for filename in glob("*.npy")} \
    if not args.learn else {}


def euclidean_dist(vector_x, vector_y):
    if len(vector_x) != len(vector_y):
        raise Exception('Vectors must be same dimensions')
    return sum((vector_x[dim] - vector_y[dim]) ** 2 for dim in range(len(vector_x)))


for frame, results in d.run():
    face_descriptor = None
    for left, top, right, bottom in results:
        rect = dlib.rectangle(left, top, right, bottom)
        shape = sp(frame, rect)
        for point in shape.parts():
            frame[point.y][point.x] = (0, 0, 255)
        face_descriptor = facerec.compute_face_descriptor(frame, shape)
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255))
        if not args.learn:
            for name, face in known_faces.items():
                dist = euclidean_dist(face_descriptor, face)
                print(dist)
                if dist < 0.2:
                    cv2.rectangle(frame, (left, bottom), (right, bottom + 40), (0, 0, 255), -1)
                    cv2.putText(frame, name, (left, bottom + 30), cv2.FONT_HERSHEY_TRIPLEX, 1, (255, 255, 255))
    cv2.imshow('previewout', frame)

    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    if key == ord(' ') and args.learn and face_descriptor is not None:
        np.save(f"{args.learn}.npy", np.array(face_descriptor))


